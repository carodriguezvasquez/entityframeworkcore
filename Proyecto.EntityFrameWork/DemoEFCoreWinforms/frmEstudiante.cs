﻿using DemoEFCoreWinforms.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoEFCoreWinforms
{
    public partial class frmEstudiante : Form
    {
        public frmEstudiante()
        {
            InitializeComponent();
        }



        /// <summary>
        /// Metodo asincronico para consultar la lista de usuarios.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnListaEstudiantes_Click(object sender, EventArgs e)
        {
            try
            {
                using (ApplicationDbContext _context = new ApplicationDbContext())
                {
                    dgvDatos.DataSource = await _context.Estudiantes.ToListAsync();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error en la aplicación: {ex.Message}.");
            }

        }

        private async void btnGuardarEstudiante_Click(object sender, EventArgs e)
        {

            Estudiante estudiante = new Estudiante()
            {
                Nombre = txtNombre.Text,
                Edad = txtEdad.Text,
            };


            try
            {
                using (ApplicationDbContext _context = new ApplicationDbContext())
                {
                    _context.Estudiantes.Add(estudiante);
                    await _context.SaveChangesAsync();
                }
                MessageBox.Show("Guardado correcto.");


            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error en la aplicación: {ex.Message}.");
            }


        }

        private async void btnEliminar_Click(object sender, EventArgs e)
        {

            try
            {
                using (ApplicationDbContext _context = new ApplicationDbContext())
                {
                         
                    Estudiante estudiante = _context.Estudiantes.Where(est =>  est.Nombre.Contains(txtNombre.Text.Trim())).FirstOrDefault();
                  
                    var entidad = new Estudiante() { estudiante.EstudianteId = id };
                    _context.Entry(entidad).State = EntityState.Deleted;
                    _context.SaveChanges();
                    if (estudiante is null)
                    {
                        MessageBox.Show("Estudiante no encontrado.");
                        return;
                    }
                
                    _context.Estudiantes.Remove(estudiante);
                    await _context.SaveChangesAsync();


                }
                MessageBox.Show("Eliminado correcto.");


            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error en la aplicación: {ex.Message}.");
            }
        }
    }
}
