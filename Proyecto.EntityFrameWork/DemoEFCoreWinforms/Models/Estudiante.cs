﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoEFCoreWinforms.Models
{
    public class Estudiante
    {
        
        public int EstudianteId { get; set; }
        
        public string Nombre { get; set; }

        public string Edad { get; set; }

        public List<EstudianteCurso> EstudianteCursos { get; set; }
    }
}
