﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoEFCoreWinforms.Models
{
    public class EstudianteCurso
    {
        [Required]
        public int EstudianteId { get; set; }

        [Required]
        public int CursoId { get; set; }

        [Required]
        public bool Estado { get; set; }


        /// <summary>
        /// Propiedades de navegación
        /// </summary>
        public Estudiante Estudiante { get; set; }
        public Curso Curso { get; set; }

    }
}
