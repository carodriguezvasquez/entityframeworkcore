﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DemoEFCoreWinforms.Migrations
{
    public partial class EstudianteCursos_Estado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Estado",
                table: "EstudianteCursos",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Estado",
                table: "EstudianteCursos");
        }
    }
}
