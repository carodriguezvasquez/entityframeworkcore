# Entity Framework Core

(EF) es un asignador relacional de objetos (ORM) que permite a los desarrolladores de .NET trabajar con datos relacionales.

En este proyecto de demostración utilizará instancia de base de datos local DB SQL Server. Es posible instalar otros [proveedores de servicios](https://docs.microsoft.com/en-us/ef/core/providers/?tabs=dotnet-core-cli) de Entity Framework Core.

Entity Framework Core 2 y 3 es compatible con proyectos que no sean core, es decir, que implementen .Net Standar 2.0; esta tecnologia es compatible .Net Framework 4.6.1+ o superior en aplicaciones desarrolladas en asp.Net MVC 5, Windows Forms, Web Forms, WCF, WPF, entre otras.

### Notas: 
1. Este ejercicio esta desarrollado en un proyecto de Windows Forms en .Net Framework **4.7**.
2. La versión de Entity Framework Core que estoy utilzando la **3.1.10**.
3. Este proyecto tiene codigo de ejemplo para crear, listar y eliminar una entidad llamado alumno.

## Instalación

#### Proveedor Sql Server
Manage Nuget packages:
`microsoft.entityframeworkcore.sqlserver`; buscar, descargar e instalar.

#### Tools Entity Framework
Manage Nuget packages:
`microsoft.entityframeworkcore.tools`; buscar, descargar e instalar.

Una vez instalada hablita [comandos](https://docs.microsoft.com/en-us/ef/core/cli/powershell) especiales desde el que permiten aplicar generar apartir de las clases las tablas y relaciones de la base de datos.
